#Overview
The Bytecode Infrastructure (BI) project is a library for processing Java (1.0-1.8) bytecode. 

It does not create a representation of it.
